package it.mytest.csv;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.opencsv.CSVReader;
import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.HeaderColumnNameTranslateMappingStrategy;
public class ReaderMapper {

	static CSVReader reader = null;
	static InputStream is = null;
	static InputStreamReader csvStreamReader = null;

	public static void main(String[] args) throws FileNotFoundException{
		
		
				Charset charset = Charset.forName("UTF-8");
					is = new FileInputStream("D:\\androidWorkspace\\csvTEst\\src\\cap_province.csv");
					csvStreamReader = new InputStreamReader(is, charset);
		
		
		    CsvToBean<CapBean> bean = new CsvToBean<CapBean>();
		
		   Map<String, String> columnMapping = new HashMap<String, String>();
		    columnMapping.put("Provincia", "CAP");
		    columnMapping.put("denoProv", "cap");
		    
		    HeaderColumnNameTranslateMappingStrategy<CapBean> strategy = 
		        new HeaderColumnNameTranslateMappingStrategy<CapBean>();
		    strategy.setType(CapBean.class);
		    strategy.setColumnMapping(columnMapping);
		    
		
		//mappa cap<-->cod_provincia
		List<CapBean> capList = bean.parse(strategy, csvStreamReader);
		System.out.println("CAPLIST.SIZE..."+capList.size());
		Map<String,String> capMap = new HashMap<String,String>();
		//crea una hashmap
		for(CapBean cp:capList){
			capMap.put(cp.getCap(), cp.getDenoProv());
		}
		System.out.println("CAPMAP.SIZE..."+capMap.size());
		
//		Set<String> keyset = capMap.keySet();
//		int i=0;
//		for(String key:keyset){
//			System.out.println(i+"...key: "+key);
//			i++;
//		}
	}
}
