package it.mytest.csv;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.apache.commons.lang3.StringUtils;

import com.opencsv.CSVReader;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.HeaderColumnNameTranslateMappingStrategy;
public class ReplaceChars {

	static CSVReader reader = null;
	static InputStream is = null;
	static InputStreamReader csvStreamReader = null;

	public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException{


		Charset charset = Charset.forName("UTF-8");
		is = new FileInputStream("D:\\androidWorkspace\\csvTEst\\src\\province.csv");
		csvStreamReader = new InputStreamReader(is, charset);

//////////////////
		
		CsvToBean<CapBean> bean = new CsvToBean<CapBean>();

		Map<String, String> columnMapping = new HashMap<String, String>();
		columnMapping.put("name", "denoProv");
		columnMapping.put("slug", "cap");

		HeaderColumnNameTranslateMappingStrategy<CapBean> strategy = 
				new HeaderColumnNameTranslateMappingStrategy<CapBean>();
		strategy.setType(CapBean.class);
		strategy.setColumnMapping(columnMapping);


		//mappa cap<-->cod_provincia
		List<CapBean> capList = bean.parse(strategy, csvStreamReader);
		System.out.println("CAPLIST.SIZE..."+capList.size());
		Map<String,String> capMap = new HashMap<String,String>();
		//crea una hashmap
		for(CapBean cp:capList){
			capMap.put(cp.getDenoProv(),cp.getCap());
		}

////////////////////
		
		Scanner scanner = new Scanner(new File("D:\\androidWorkspace\\csvTEst\\src\\cap_province.csv"));
		scanner.useDelimiter("\n");
		PrintWriter writer = new PrintWriter("D:\\androidWorkspace\\csvTEst\\src\\listaCorretta.csv", "UTF-8");
		while (scanner.hasNextLine()) {
			String data = scanner.next();
			String[] splitted = data.split(",");
			String deno = splitted[0].trim();
			String cap = splitted[1].trim();
			String slug = capMap.get(deno);
			if(StringUtils.isEmpty(slug)){
				System.out.println("SLUG NOT FOUND FOR..."+deno);
			}
			String newRow = slug+","+cap;
			writer.println(newRow);
		}
		scanner.close();
		writer.close();
	}
}
